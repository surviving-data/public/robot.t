// console.log = function log() {
//     fs.writeSync(this._stdout.fd, util.format.apply(null, arguments) + '\n')
// }
const util = require('util')
const glob = util.promisify(require('glob'))
const { localesPath } = require('./variables')

export function sample(choices: any[]): any {
    var index = Math.floor(Math.random() * choices.length)
    return choices[index]
}

export function randFloat(min: number, max: number): number {
    return Math.random() * (max - min) + min
}

export const getLanguages = async () => {
    const globPattern: string = `${localesPath}/**/*.po`
    const locales: string[] = await glob(globPattern, {})
    return locales.map((locale) => locale.split('/').pop().split('.')[0])
}

export const getLocale = async (language: string) => {
    let locale = `${language}`.toLowerCase()
    // Exception for chinese in simplfied mode
    if (['zh_CN', 'cn'].includes(language)) {
        locale = 'zh-cn'
    } else {
        locale = language.split('_')[0]
    }
    return locale
}
// export const languages = process.env.languages ?? ['fr-FR', 'en-GB', 'en-US', 'it-IT', 'de-DE']
