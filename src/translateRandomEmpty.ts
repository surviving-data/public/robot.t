const translate = require('@iamtraction/google-translate')
const gettextParser = require('gettext-parser')
const fs = require('fs')

const { localesPath, waitThresholdDefault, waitThresholdAfterBannishment } = require('./variables')
const { sample, getLocale, getLanguages } = require('./utils')

let waitThreshold = waitThresholdDefault

interface Message {
    msgid: string
    comments: Object[]
    msgstr: string[]
}

// Translate one random empty  msgstr from on random po file
export const translateRandomEmpty = async () => {
    // Parse a random po file content with gettextParser https://www.npmjs.com/package/gettext-parser
    let language = sample(await getLanguages())
    const poFile = `${localesPath}/${language}.po`
    const po = gettextParser.po.parse(fs.readFileSync(poFile))

    if (!po.headers) {
        po.headers = {
            'Project-Id-Version': 'PACKAGE VERSION',
            'Report-Msgid-Bugs-To': '',
            'POT-Creation-Date': '2022-05-16 21:23+0000',
            'PO-Revision-Date': '2022-05-16 19:35+0000',
            'Last-Translator': 'Automatically generated',
            'Language-Team': 'none',
            Language: language,
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=utf-8',
            'Content-Transfer-Encoding': '8bit',
            'Plural-Forms': 'nplurals=2; plural=(n > 1);',
        }
    }

    language = po.headers?.Language ?? language
    po.headers.Language = language

    // Get a random message that have an empty msgstr
    const translations = po.translations?.[''] ?? {}
    const message = sample(Object.values(translations).filter((message: Message) => message.msgstr[0] == ''))
    if (!message?.msgid) return null

    try {
        // translate uses google translate
        const to = await getLocale(language)
        // TODO: make the default msgid language dynamic
        if (to != 'en') {
            //
            const res = await translate(message.msgid, { from: 'en', to })
            message.msgstr = [res.text || res.from.text.value]

            // Write the new po
            console.log(`Translated [en]: ${message.msgid} => [${to}]:${message.msgstr[0]}`)
            // const buffer =
            fs.writeFileSync(poFile, gettextParser.po.compile(po))

            // Set the default waiting threshold
            waitThreshold = waitThresholdDefault

            return message.msgstr
        }
    } catch (err) {
        // Increase the waiting threshold each bannishment
        waitThreshold = waitThresholdAfterBannishment
        console.error(err)
    }
}
